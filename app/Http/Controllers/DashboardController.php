<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = view('admin.news.index');
        $view->news = News::orderBy('id', 'desc')->paginate(10);
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:news|max:255',
            'content' => 'required',
        ]);

        DB::beginTransaction();

        $news = new News;
        $news->title = $request->get('title');
        $news->slug = str_slug($request->get('title'));
        $news->short_description = $request->get('short_description');
        $news->content = $request->get('content');
        $news->save();

        $file = $request->file('file');
        if ($file) {
            $ext = $file->guessClientExtension();
            $path = $file->storeAs('public/news-thumbnail', $news->id . '.' . $ext);
            $news->thumbnail = $path;
            $news->save();
        }

        DB::commit();

        return redirect('/admin/dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $view = view('admin.news.edit');
        $view->newsSingle = News::findOrFail($id);
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|unique:news,title,'.$id.'|max:255',
            'content' => 'required',
        ]);

        DB::beginTransaction();

        $news = News::findOrFail($id);
        $news->title = $request->get('title');
        $news->slug = str_slug($request->get('title'));
        $news->short_description = $request->get('short_description');
        $news->content = $request->get('content');
        $news->save();

        $file = $request->file('file');
        if ($file) {
            $ext = $file->guessClientExtension();
            $path = $file->storeAs('public/news-thumbnail', $news->id . '.' . $ext);
            $news->thumbnail = $path;
            $news->save();
        }

        DB::commit();

        return redirect('/admin/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newsSingle = News::findOrFail($id);

        Storage::delete($newsSingle->thumbnail);

        $newsSingle->delete();

        return redirect('/admin/dashboard');
    }
}
