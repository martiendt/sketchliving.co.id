<?php

namespace App\Http\Controllers\Landing;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = view('landing.news.index');
        $view->news = News::orderBy('id', 'desc')->paginate(5);
        return $view;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $view = view('landing.news.show');
        $view->newsSingle = News::where('slug', $slug)->first();
        $view->news = News::orderBy('id', 'desc')->paginate(5);
        return $view;
    }
}
