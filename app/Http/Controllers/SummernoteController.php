<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SummernoteController extends Controller
{
    public function imageUpload(Request $request)
    {
        $file = $request->file('file');

        if ($file) {
            $ext = $file->guessClientExtension();
            $fileName = Str::random(40).'.'.$ext;
        }

        $allowed = array('png', 'jpg', 'jpeg', 'gif');
        if(isset($_FILES['file']) && $_FILES['file']['error'] == 0){
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            if(!in_array(strtolower($extension), $allowed)){
                exit;
            }
            if(move_uploaded_file($_FILES['file']['tmp_name'],public_path().'/uploads/news/'.$fileName)){
                echo '/uploads/news/'.$fileName;
                exit;
            }
        }
    }
}
