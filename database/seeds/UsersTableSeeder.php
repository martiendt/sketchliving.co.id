<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User;
        $user->name = 'admin';
        $user->email = 'admin@sketchliving.co.id';
        $user->password = bcrypt('sketch2017');
        $user->save();
    }
}
