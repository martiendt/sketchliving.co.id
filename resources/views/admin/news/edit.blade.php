@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>News</h1>

            <a class="btn" href="{{ url('admin/dashboard') }}">List</a>
            <a class="btn" href="{{ url('admin/dashboard/create') }}">Create</a>

            <hr>

            <form method="post" action="{{ url('admin/dashboard/'.$newsSingle->id) }}" enctype="multipart/form-data">
                {!! method_field('PUT') !!}
                {!! csrf_field() !!}
                <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label">Thumbnail</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" name="file" id="file">
                        <hr>
                        @if(Storage::exists($newsSingle->thumbnail))
                            <img src="{{ Storage::url($newsSingle->thumbnail) }}" alt="" height="300px">
                        @endif
                        <hr>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="{{ $newsSingle->title }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="content" class="col-sm-2 col-form-label">Short Description</label>
                    <div class="col-sm-10">
                        <textarea rows="6" name="short_description" class="form-control">{{ $newsSingle->short_description }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="content" class="col-sm-2 col-form-label">Content</label>
                    <div class="col-sm-10">
                        <textarea name="content" id="content">{!! $newsSingle->content !!}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="content" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                        <input type="submit" class="btn btn-primary" value="Update"/>
                    </div>
                </div>
            </form>
            <form method="post" action="{{ url('admin/dashboard/'.$newsSingle->id) }}" onsubmit="return confirm('Are you sure want to delete this news ?')">
                {!! method_field('DELETE') !!}
                {!! csrf_field() !!}
                <div class="form-group row">
                    <label for="content" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                        <input type="submit" class="btn btn-danger" value="Delete" />
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
      $(document).ready(function() {
        $('#content').summernote({
          height: 300,
          callbacks: {
            onImageUpload: function(files, editor, $editable) {
              sendFile(files[0],editor,$editable);
            }
          }
        });
      });

      function sendFile(file,editor,welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
          url: "/summernote/image-upload",
          data: data,
          cache: false,
          contentType: false,
          processData: false,
          type: 'POST',
          success: function(data){
            $('#content').summernote("insertImage", data, 'filename');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus+" "+errorThrown);
          }
        });
      }
    </script>
@endsection
