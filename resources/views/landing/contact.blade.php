@extends('landing.layouts.master')

@section('content')
    <section id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-center">
                    <h3 class="uppercase color-primary mb40 mb-xs-24">Contact Us</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="image-square left">
        <div class="col-md-6 image">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/arch6.jpg" />
            </div>
        </div>
        <div class="col-md-6 col-md-offset-1 content">
            <h3 class="uppercase color-primary">Get In Touch</h3>
            <hr>
            <p>
                Babatan Pratama XIII QQ/55
                <br /> Surabaya, Indonesia
            </p>
            <p>
                <strong>Phone:</strong> +6231 753 5385
                <br />
                <strong>Mobile:</strong> +6282 2444 12970
                <br />
            </p>
            <hr>
            <ul class="list-inline social-list">
                <li>
                    <a href="https://www.facebook.com/Sketsa-Cipta-Graha-227056494497125" target="_blank">
                        <i class="ti-facebook"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/sketchcontractor" target="_blank">
                        <i class="ti-instagram"></i>
                    </a>
                </li>
            </ul>
        </div>
    </section>
@endsection
