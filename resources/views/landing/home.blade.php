@extends('landing.layouts.master')

@section('content')
    <section class="kenburns cover fullscreen image-slider slider-arrow-controls controls-inside">
        <ul class="slides">
            <li class="image-bg pt-xs-240 pb-xs-240">
                <div class="background-image-holder">
                    <img alt="image" class="background-image" src="{{ asset('img/home/proyek-1.jpg') }}" />
                </div>
                <div class="align-bottom">
                    <div class="row">
                        <div class="col-sm-12">
                            <hr class="mb24">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12 text-center-xs mb-xs-24">
                            <h4 class="uppercase mb0 bold">SKETCHLIVING</h4>
                            <span>100+ Proyek Komersial & Residential</span>
                        </div>
                        <div class="col-md-6 hidden-sm hidden-xs">
                            <p>
                                Menerapkan pembangunan gedung sesuai dengan project timeline dan budget yang efisien untuk menghasilkan kualitas yang terbaik.
                            </p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 text-right text-center-xs">
                            <a class="btn btn btn-white mt16" href="{{ url('/contact') }}">Contact Us</a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </li>
            <li class="image-bg pt-xs-240 pb-xs-240">
                <div class="background-image-holder">
                    <img alt="image" class="background-image" src="{{ asset('img/home/proyek-2.jpg') }}" />
                </div>
                <div class="align-bottom">
                    <div class="row">
                        <div class="col-sm-12">
                            <hr class="mb24">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12 text-center-xs mb-xs-24">
                            <h4 class="uppercase mb0 bold">SKETCHLIVING</h4>
                            <span>100+ Proyek Komersial & Residential</span>
                        </div>
                        <div class="col-md-6 hidden-sm hidden-xs">
                            <p>
                                Menerapkan pembangunan gedung sesuai dengan project timeline dan budget yang efisien untuk menghasilkan kualitas yang terbaik.
                            </p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 text-right text-center-xs">
                            <a class="btn btn btn-white mt16" href="{{ url('/contact') }}">Contact Us</a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </li>
            <li class="image-bg pt-xs-240 pb-xs-240">
                <div class="background-image-holder">
                    <img alt="image" class="background-image" src="{{ asset('img/home/proyek-3.jpg') }}" />
                </div>
                <div class="align-bottom">
                    <div class="row">
                        <div class="col-sm-12">
                            <hr class="mb24">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12 text-center-xs mb-xs-24">
                            <h4 class="uppercase mb0 bold">Sketchliving</h4>
                            <span>100+ Proyek Komersial & Residential</span>
                        </div>
                        <div class="col-md-6 hidden-sm hidden-xs">
                            <p>
                                Menerapkan pembangunan gedung sesuai dengan project timeline dan budget yang efisien untuk menghasilkan kualitas yang terbaik.
                            </p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 text-right text-center-xs">
                            <a class="btn btn btn-white mt16" href="{{ url('/contact') }}">Contact Us</a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </li>
        </ul>
    </section>
    <section class="bg-dark" id="company-overview">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 text-center">

<img class="logo" alt="Sketchliving" src="{{ asset('img/logo-sketch.webp') }}" width="300px"/>                    <h3 class="uppercase">
                        Company Overview
                    </h3>                    
                </div>
            </div>
            <!--end of row-->
            <div class="row mb80 mb-xs-24">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <p>
                        "Jika kita tidak pernah berani untuk mencoba, maka kita tidak akan pernah tahu kesempatan besar yang akan membuat diri kita maju." Prinsip itulah yang membuat kami percaya diri untuk melangkah bersama membangun PT. Sketsa Cipta Graha.
                    </p>
                    <p>
                        Berawal dari tahun 2007 yang diawali dengan sebuah usaha kontraktor bangunan berkantor di Surabaya Barat, kami merintis dan membangun pengalaman arsitektural baik di bidang residensial dan komersial.
                    </p>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-sm-5 col-sm-offset-1 text-center">
                    <div class="feature boxed">
                        <h1 class="large">60+</h1>
                        <h5 class="uppercase">Proyek Residensial</h5>
                    </div>
                </div>
                <div class="col-sm-5 text-center">
                    <div class="feature boxed">
                        <h1 class="large">20+</h1>
                        <h5 class="uppercase">Proyek Komersial</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="our-project">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-center">
                    <h3 class="uppercase color-primary mb40 mb-xs-24">Our Project</h3>
                    <p class="lead">
                        Keinginan kami untuk bertumbuh menjadi sebuah kontraktor besar di tanah air ternyata mendapat sokongan dari para pendiri. Sehingga pada tahun 2007 kami membentuk Sketch Construction.
                    </p>
                    <p class="lead">
                        Sketch Construction mengutamakan kepuasan pelanggan dan menjaga relasi hingga kedepannya. Sketch Construction juga sudah berpengalaman menggarap puluhan proyek baik di Surabaya maupun luar kota. Percayakan konstruksi anda kepada yang berpengalaman dan profesional untuk hasil yang lebih maksimal.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="projects p0 bg-dark">
        <div class="row masonry-loader pt80">
            <div class="col-sm-12 text-center">
                <div class="spinner"></div>
            </div>
        </div>
        <div class="row masonry masonryFlyIn">
            <div class="col-sm-6 masonry-item project" data-filter="People">
                <div class="image-tile hover-tile text-center">
                    <img alt="image" class="background-image" src="img/project/residential.jpg" />
                    <div class="hover-state">
                        <a href="{{ url('/projects/residential') }}">
                            <h3 class="uppercase mb8">Residential</h3>
                        </a>
                    </div>
                </div>
                <!--end of hover tile-->
            </div>
            <div class="col-sm-6 masonry-item project" data-filter="People">
                <div class="image-tile hover-tile text-center">
                    <img alt="image" class="background-image" src="img/project/commercial.jpg" />
                    <div class="hover-state">
                        <a href="{{ url('/projects/commercial') }}">
                            <h3 class="uppercase mb8">Commercial</h3>
                        </a>
                    </div>
                </div>
                <!--end of hover tile-->
            </div>
            <div class="col-sm-6 masonry-item project" data-filter="People">
                <div class="image-tile hover-tile text-center">
                    <img alt="image" class="background-image" src="img/project/developer.jpg" />
                    <div class="hover-state">
                        <a href="{{ url('/projects/developer') }}">
                            <h3 class="uppercase mb8">Developer</h3>
                        </a>
                    </div>
                </div>
                <!--end of hover tile-->
            </div>
        </div>
        <!--end of row-->
    </section>
    <section id="contact-us" class="bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-center">
                    <h3 class="uppercase color-white mb40 mb-xs-24">Contact Us</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="image-square left">
        <div class="col-md-6 image">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/arch6.jpg" />
            </div>
        </div>
        <div class="col-md-6 col-md-offset-1 content">
            <h3 class="uppercase color-primary">Get In Touch</h3>
            <hr>
            <p>
                Babatan Pratama XXIII QQ/55
                <br /> Surabaya, Indonesia
            </p>
            <p>
                <strong>Phone:</strong> +6231 753 5385
                <br />
                <strong>Mobile:</strong> +6282 2444 12970
                <br />
            </p>
            <hr>
            <ul class="list-inline social-list">
                <li>
                    <a href="https://www.facebook.com/Sketsa-Cipta-Graha-227056494497125" target="_blank">
                        <i class="ti-facebook"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/sketchcontractor" target="_blank">
                        <i class="ti-instagram"></i>
                    </a>
                </li>
            </ul>
        </div>
    </section>

@endsection
