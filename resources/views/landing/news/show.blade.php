@extends('landing.layouts.master')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-9 mb-xs-24">
                    <div class="post-snippet mb64">
                        <a href="#">
                            {{--<img class="mb24" alt="Post Image" src="img/blog-single-4.jpg" />--}}
                        </a>
                        <div class="post-title">
                            <span class="label">{{ $newsSingle->created_at->format('d M') }}</span>
                            <a href="#">
                                <h4 class="inline-block">{{ $newsSingle->title }}</h4>
                            </a>
                        </div>

                        <hr>

                        {!! $newsSingle->content !!}
                    </div>
                </div>

                @include('landing.news.sidebar', ['news' => $news])
            </div>
        </div>
    </section>
@endsection
