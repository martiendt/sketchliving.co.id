@extends('landing.layouts.master')

@section('content')
    <section id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-center">
                    <h3 class="uppercase color-primary mb0 mb-xs-24">Projects</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="projects pt0">
        <div class="container">
            <div class="row pb24">
                <div class="col-sm-12 text-center">
                    <ul class="filters mb0">
                        <a class="btn btn-lg mb0" href="{{ url('/projects/commercial') }}">Commercial</a>
                        <a class="btn btn-filled btn-lg mb0" href="{{ url('/projects/residential') }}">Residential</a>
                        <a class="btn btn-lg mb0" href="{{ url('/projects/developer') }}">Developer</a>
                    </ul>
                </div>
            </div>
            <br/>
            <!--end of row-->
            <div class="row masonry-loader">
                <div class="col-sm-12 text-center">
                    <div class="spinner"></div>
                </div>
            </div>
            <div class="row masonry masonryFlyIn">
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Residential">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="img/residential/dian-istana-c4-11/main.jpg" />
                            <div class="title">
                                <h5 class="uppercase mb0">Dian Istana C4-11</h5>
                                <span>Residential</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Residential">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="img/residential/graha-b2/main.jpg" />
                            <div class="title">
                                <h5 class="uppercase mb0">Graha B2</h5>
                                <span>Residential</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Residential">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="img/residential/graha-m58/main.jpg" />
                            <div class="title">
                                <h5 class="uppercase mb0">Graha M58</h5>
                                <span>Residential</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Residential">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="img/residential/graha-r58/main.jpg" />
                            <div class="title">
                                <h5 class="uppercase mb0">Graha R58</h5>
                                <span>Residential</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Residential">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="img/residential/imperial-beach-f10-16/main.jpg" />
                            <div class="title">
                                <h5 class="uppercase mb0">Imperial Beach F10-16</h5>
                                <span>Residential</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Residential">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="img/residential/rejomulyo-kediri/main.jpg" />
                            <div class="title">
                                <h5 class="uppercase mb0">Rejomulyo Kediri</h5>
                                <span>Residential</span>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection
