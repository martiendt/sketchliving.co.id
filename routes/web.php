<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing.home');
});

Route::get('/company-overview', function () {
    return view('landing.company-overview');
});

Route::get('/projects', function () {
    return view('landing.projects');
});

Route::get('/projects/{category}', function ($category) {
    return view('landing.projects.'.$category);
});

Route::get('/contact', function () {
    return view('landing.contact');
});

Route::post('/summernote/image-upload', 'SummernoteController@imageUpload');

Route::namespace('Landing')->group(function () {
    Route::resource('/news', 'NewsController');
});

Route::prefix('admin')->group(function () {
    Auth::routes();
    Route::get('/', function () {
        return redirect('admin/login');
    });
    Route::resource('/dashboard', 'DashboardController');
});


